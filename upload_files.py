import os
import pysftp
import secrets
from tqdm import tqdm


def upload_files():
    with pysftp.Connection(secrets.hostname, username=secrets.hostuser, password=secrets.hostpassword) as sftp:
        print("Connected to Server")
        print("Starting file upload")
        files = os.listdir(secrets.downloaddir)
        for f in tqdm(files):
            # if the file is an episode
            season = ""
            if "_S" in f:
                season = "/Staffel " + f.split("_S")[1].split("_")[0]

            if "_E" in f:
                series_name = f.split("_")[0]

                subpath = "/Serien/"+series_name + season

            else:  # if it is a movie
                subpath = "/Filme"
            sftp.makedirs(secrets.uploadpath + subpath)
            with sftp.cd(secrets.uploadpath + subpath):
                sftp.put(secrets.downloaddir+"/" + f)
                os.remove(secrets.downloaddir + "/" + f)

    print("All files uploaded. Deleting local copies")


if __name__ == "__main__":
    upload_files()
