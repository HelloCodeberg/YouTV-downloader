from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.options import Options
import re
import secrets
import time
import os
from rename_videos import rename_videos
from upload_files import upload_files

options = Options()
options.headless = True

profile = webdriver.FirefoxProfile()
profile.set_preference('browser.download.folderList', 2)  # custom location
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', secrets.downloaddir)
profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'video/mp4')

driver = webdriver.Firefox(profile, options=options)
driver.get("https://www.youtv.de/videorekorder")
time.sleep(2)
cookiebtn = driver.find_element(
    by=By.XPATH, value="/html/body/div[2]/div/div/button[1]")
cookiebtn.click()

email_field = driver.find_element_by_id("session_email")
email_field.send_keys(secrets.username)

pw_field = driver.find_element_by_id("session_password")
pw_field.send_keys(secrets.password)

login_button = driver.find_element_by_xpath(
    "/html/body/section/div/div/div[2]/div[1]/form/div[3]/input")
login_button.click()

time.sleep(2)
titles = driver.find_elements_by_class_name("broadcasts-table-cell-title")
episodes = driver.find_elements_by_class_name("broadcasts-table-cell-info")
dates = driver.find_elements_by_class_name("broadcasts-table-cell-date")

downloads = driver.find_elements_by_class_name("action-download")
deletes = driver.find_elements_by_class_name("action-delete")
try:
    titles.pop(0)
except Exception:
    print("No new videos")
    exit()
episodes.pop(0)
dates.pop(0)

for i, recording in enumerate(titles):
    title = recording.text.split("\n")[0]
    episode_text = episodes[i].text
    season = ""
    episode = ""
    try:
        season = re.search('Staffel (.*) -', episode_text).group(1)
    except Exception:
        pass
    try:
        episode = re.search("Episode (.*)\n", episode_text).group(1)
    except Exception as e:
        print(e)
    if season != "":
        title = title + "_S{0}".format(season)
    if episode != "":
        title = title + "_E{0}".format(episode)
    if episode == "" and season == "":
        title = recording.text
    title = title + ".mp4"
    # replace illegal characters
    title.replace(":", "-")
    title.replace("&", " und ")
    title.replace("#", "")
    title.replace("+", "plus")

    filepath = secrets.downloaddir + "/" + dates[i].text
    f = open(filepath, "w")
    f.write(title)
    f.close()

    downloads[i].click()
    download = None
    try:
        download = driver.find_element_by_xpath(
            "//li[@title='Sendung in HD High Definition herunterladen']")
    except Exception:
        try:
            download = driver.find_element_by_xpath(
                "//li[@title='Sendung in HQ Hohe Qualität herunterladen']")
        except Exception:
            download = driver.find_element_by_xpath(
                "//li[@title='Sendung in NQ Normale Qualität herunterladen']")

    download.click()
    action = webdriver.ActionChains(driver)
    time.sleep(5)

    action.move_by_offset(10, 20)    # 10px to the right, 20px to bottom
    action.perform()

for delete in deletes:
    delete.click()
    WebDriverWait(driver, 10).until(EC.alert_is_present())
    driver.switch_to.alert.accept()
time.sleep(60)
driver.quit()
rename_videos()
upload_files()
