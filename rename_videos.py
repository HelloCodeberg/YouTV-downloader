import os
import secrets
from datetime import datetime


def rename_videos():
    # read files which contain video and titles
    files = os.listdir(secrets.downloaddir)
    raw_titles = []
    titles = []
    videos = []

    for f in files:
        if f[2] == ".":
            with open(secrets.downloaddir + "/"+f, "r") as text_file:
                text = text_file.read()
                text_file.close()

                text = text.replace("\n", " ")
                titles.append(text)
                raw_titles.append(f)
        else:
            videos.append(f)

    # Rename files
    for i, video_title in enumerate(videos):
        parts = video_title.split("_")
        date = datetime.strptime(parts[0], '%Y-%m-%d')
        time = datetime.strptime(parts[1], '%H-%M')

        for j, t in enumerate(raw_titles):
            parts = t.split(" - ")
            date2 = datetime.strptime(parts[0], '%d.%m.%y')
            time2 = datetime.strptime(parts[1].split("\n")[0], '%H:%M')
            if date == date2 and time == time2:
                f1 = secrets.downloaddir + "/" + video_title
                f2 = secrets.downloaddir + "/" + titles[j]
                print("Renaming: {0}".format(f2))
                os.rename(secrets.downloaddir + "/" + video_title,
                          secrets.downloaddir + "/" + titles[j])
                break

    print("All files renamed")
    for name in files:
        if not name.endswith(".mp4"):
            os.remove(secrets.downloaddir + "/" + name)


if __name__ == "__main__":
    rename_videos()
